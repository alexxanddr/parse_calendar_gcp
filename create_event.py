from datetime import datetime, timedelta
from cal_setup import get_calendar_service
from login_controller import get_calendar 
import re,json

TAG_RE = re.compile(r'<[^>]+>')

def remove_tags(text):
    return TAG_RE.sub('', text)

def main():
   # creates one hour event tomorrow 10 AM IST
  service = get_calendar_service()
  cal = get_calendar()

  distros_dict = json.loads(cal) 
  for event in distros_dict:
    print(event)
    event_result = service.events().insert(calendarId='upp58fg0cfgb65m7lp44sgchqc@group.calendar.google.com',
        body={
            "summary": event['title'],
            "description": remove_tags(event['tooltip']),
            "start": {"dateTime": event['start'], "timeZone": 'Europe/Rome'},
            "end": {"dateTime": event['end'], "timeZone": 'Europe/Rome'},
        }
    ).execute()

if __name__ == '__main__':
   main()
