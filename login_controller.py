import requests,json

def get_calendar():

    #url = "https://itsar.registrodiclasse.it/geopcfp2/update/login.asp"    
    querystring = {"1":"1","ajax_target":"DIVHidden","ajax_tipotarget":"login"}
    
    #payload = "EMAIL&password=PIPPO&=Login"
    headers = {
        'Sec-Fetch-Mode': "cors",
        'Sec-Fetch-Site': "same-origin",
        'Origin': "https://itsar.registrodiclasse.it",
        'Accept-Encoding': "gzip, deflate, br",
        'Accept-Language': "it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7",
        'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
        'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8",
        'Accept': "*/*",
        'Referer': "https://itsar.registrodiclasse.it/geopcfp2/",
        'Connection': "keep-alive",
        'DNT': "1",
        'cache-control': "no-cache"
        }
    
    session = requests.Session()
    
    response = session.request("POST", url, data=payload, headers=headers, params=querystring)
    
    session_code = session.cookies.get_dict() 
    
    tcode= json.dumps(session_code)
    
    loaded_json = json.loads(tcode)
    
    result = ""
    
    for x in loaded_json:
            result=x+'='+loaded_json[x]
    
    
    ## Get Calendar
    
    url_calendar = "https://itsar.registrodiclasse.it/geopcfp2/json/fullcalendar_events_alunno.asp"
    
    querystring_calendar = {"Oggetto":"idAlunno","idOggetto":"965","editable":"false","z":"1569495411945","start":"2019-08-26","end":"2020-01-31","_":"1569495411057"}
    
    headers_calendar = {
        'Connection': "keep-alive",
        'Cache-Control': "max-age=0",
        'Upgrade-Insecure-Requests': "1",
        'User-Agent': "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
        'Sec-Fetch-Mode': "navigate",
        'Sec-Fetch-User': "?1",
        'DNT': "1",
        'Accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        'Sec-Fetch-Site': "none",
        'Accept-Encoding': "gzip, deflate, br",
        'Accept-Language': "it-IT,it;q=0.9,en-US;q=0.8,en;q=0.7",
        'Cookie': result+";",
        'cache-control': "no-cache"
        }
    
    response_calendar = requests.request("GET", url_calendar, headers=headers_calendar, params=querystring_calendar)
    
    return(response_calendar.text)

