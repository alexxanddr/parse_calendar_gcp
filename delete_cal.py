from cal_setup import get_calendar_service
import datetime

def get_events():
   service = get_calendar_service()
   # Call the Calendar API
   now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
   print(now)
   print('Getting List o 10 events')
   events_result = service.events().list(
       calendarId='upp58fg0cfgb65m7lp44sgchqc@group.calendar.google.com', timeMin=now,
       maxResults=9999999, singleEvents=True,
       orderBy='startTime').execute()
   events = events_result.get('items', [])

   if not events:
       print('No upcoming events found.')
   for event in events:
       ids=event['id']
       try:
           service.events().delete(
               calendarId='upp58fg0cfgb65m7lp44sgchqc@group.calendar.google.com',
               eventId=ids,
           ).execute()
       except Exception:
           print("Failed to delete event")

       print("Event deleted")



def main():
   # Delete the event
    get_events()
#       service.events().delete(
#           calendarId='primary',
#           eventId='<place your event ID here>',
#       ).execute()

if __name__ == '__main__':
   main()
